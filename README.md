# Eco Trip TDD React Page w/ custom webpack/ babel config

My first React act. Custom built webpack dev environment after facing enormous frustration with `create-react-app` (as of March
2023 [no longer the official way of starting projects](https://dev.to/ag2byte/create-react-app-is-officially-dead-h7o)).

Task details can be found in `FE task.pdf`.

## Run instructions

Run `npm i` in the project root to gather all the dependencies.

### Development

To run in development mode using `webpack-dev-server` run `npm run dev`. This will spin up the express API on port `3001` and the
dev-server (opened automagically on port `9500`).

### Production mode

Build the project using webpack, use `npm run build`. This will compile & bundle all the `.ts|.js` into ES2022 browser friendly
JS, CSS & HTML in the `dist` dir. Once built run `npm run prod` to serve the web application using `serve` and express server
(you'll need to visit `http://localhost:3000` manually).

Alternatively you can just open the `index.html` compiled in `dist` with the `server` running and it will display as intended.

## Building & Serving

`npm run build` to compile & bundle the src files into one serveable chunk in the `./dist` dir `npx serve dist` to serve the
production application from the `./dist/` dir

## Known issues

Note: I struggled a bit with familiarising myself with Chakra & React (especially the webpack + babbel toolset at the beginning)
which meant far more hours were spent reading documentation vs hands-on coding. Although this hampered workflow, I still enjoyed
working with this toolchain and am keen to learn more.

### Front end

-   Not nearly as much testing as I would have ideally liked.
-   Background image border – is a square inside a round box. Requires nesting the background box in another box
-   Fonts & Text – Sizing and their absolute dimensions are off at times in non-standard screen dimensions
-   Positioning - the replication of the required task is not perfect sadly, some margin/padding/spacing could be tweaked
-   Colours – Same applies to above
-   If the BackgroundURL doesn't load correctly through Chakra the box looks ugly
-   Tests are not as thorough as ideally desired.
-   Tests is asserting the subpar functioning of the StarIcons rounding down

### Back end

-   Gracefully killing the Express server – liberates the port but leaves a process lingering
-   Using `concurently` to run both "servers" concurently in the `npm run (dev|prod)` tasks

# Future considerations

-   `turbo` Monorepo for better code sharing, optimised parallelised tasks, and just a superior dev workflow
-   `docker` for deploying backend in a reproducable environment
-   `dotenv` for Env Vars
-   `rxJS` for more elegant handling of API Data Streams (rather than fault-prone `Promise`s)
-   `Cypress` E2E tests once interaction had been put in place
-   `BEM-style stylesheets` Something to refactor the crazy CSS duplication present in the `TripComponent`. In past experience
    using `BEM` has been quite clear
-   Dynamically built `.svg` icons for displaying trip rating fractions
