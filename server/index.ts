import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import { DummyTripData } from "./dummy_eco_trips";

const { json, urlencoded } = bodyParser;

const createServer = () => {
    const app = express();
    app.disable("x-powered-by")
        .use(urlencoded({ extended: true }))
        .use(cors())
        .use(json())
        .get("/api/trips", (_, res) => {
            console.log("[server] GET /api/trips");
            return res.json(JSON.stringify(DummyTripData));
        });

    return app;
};

const port = process.env.PORT || 3001;
const server = createServer();

server.listen(port, () => {
    console.log(`api running on ${port}`);
});
