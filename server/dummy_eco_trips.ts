// ugly duplication, could extract if time permits
export interface Trip {
    id: number;
    backgroundImgUrl: string;
    title: string;
    duration: number;
    nbCountries: number;
    emissionsOffset: number;
    tripRating: number;
}

export const DummyTripData: Trip[] = [
    {
        id: 123,
        backgroundImgUrl:
            "https://hips.hearstapps.com/hmg-prod/images/long-boat-docked-on-beach-in-krabi-thailand-summers-royalty-free-image-1622044679.jpg",
        title: "Nouvelle-Zélande",
        duration: 3,
        nbCountries: 1,
        emissionsOffset: 102,
        tripRating: 4,
    },
    {
        id: 456,
        backgroundImgUrl: "https://imageio.forbes.com/specials-images/dam/imageserve/1171238184/0x0.jpg?format=jpg&width=1200",
        title: "Amazon Experience",
        duration: 9,
        nbCountries: 6,
        emissionsOffset: 10382,
        tripRating: 3.7,
    },
    {
        id: 789,
        backgroundImgUrl:
            "https://images.unsplash.com/photo-1433838552652-f9a46b332c40?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8NHx8fGVufDB8fHx8&w=1000&q=80",
        title: "China",
        duration: 22,
        nbCountries: 1,
        emissionsOffset: 102,
        tripRating: 4,
    },
    {
        id: 101,
        backgroundImgUrl:
            "https://media.istockphoto.com/id/1396220896/photo/young-woman-traveler-relaxing-and-enjoying-at-beautiful-tropical-white-sand-beach-at-maya-bay.jpg?b=1&s=170667a&w=0&k=20&c=kP4x6-edD2p5fEAjfekOSTITfYv915BAKzkJcgwL2Lw=",
        title: "Philippine boat adventure",
        duration: 7,
        nbCountries: 3,
        emissionsOffset: 50,
        tripRating: 4.9,
    },
];
