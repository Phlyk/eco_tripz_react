import { render } from "@testing-library/react";
import { App } from "../src/App";

jest.mock("../src/trip/TripList.Component", () => () => {
    return <div data-testid="trip-list-component-stub" />;
});

describe("App tests", () => {
    it("should display only the TripListComponent on App Page", () => {
        const { queryByTestId } = render(<App />);
        expect(queryByTestId("trip-list-component-stub")).toBeTruthy();
        expect(queryByTestId("trip-component-stub")).toBeFalsy();
    });
});
