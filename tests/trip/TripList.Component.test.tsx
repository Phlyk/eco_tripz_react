import { act, render } from "@testing-library/react";
import TripListComponent from "../../src/trip/TripList.Component";
import { Trip } from "../../src/trip/Trip";

const mockTripComponent = jest.fn();
jest.mock("../../src/trip/Trip.Component", () => (props: Trip) => {
    mockTripComponent(props);
    return <div data-testid="trip-component-stub" />;
});

const testTrip1 = {
    id: 1,
    title: "Trip 1",
    nbCountries: 1,
    duration: 1,
    emissionsOffsetUnit: 1,
    emissionsUnit: "kg",
    tripRating: 1,
};
const testTrip2 = {
    id: 2,
    title: "Trip 2",
    nbCountries: 2,

    duration: 2,
    emissionsOffsetUnit: 2,
    emissionsUnit: "kg",
    tripRating: 2,
};
jest.mock("../../src/trip/Trip.Service", () => {
    return {
        getTrips: () => {
            return [testTrip1, testTrip2];
        },
    };
});

describe("TripList Component Tests", () => {
    it("should instantiate N number of TripComponents with corresponding TripData", async () => {
        await act(async () => render(<TripListComponent />));
        expect(mockTripComponent).toHaveBeenCalledTimes(2);
        expect(mockTripComponent).toHaveBeenCalledWith(testTrip1);
        expect(mockTripComponent).toHaveBeenCalledWith(testTrip2);
    });
});
