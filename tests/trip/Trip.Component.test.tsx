import { render } from "@testing-library/react";
import TripComponent from "../../src/trip/Trip.Component";

const mockChakraIcon = jest.fn();
jest.mock("@chakra-ui/icons", () => ({
    StarIcon: (props: any) => {
        mockChakraIcon(props);
        return <div data-testid="star-icon-stub" />;
    },
}));

describe("TripComponent tests", () => {
    beforeEach(() => {
        mockChakraIcon.mockClear();
    });

    it("Should rendrer 5 stars in total", () => {
        const renderResult = render(
            <TripComponent
                id={0}
                backgroundImgUrl={""}
                title={""}
                duration={0}
                nbCountries={0}
                emissionsOffset={0}
                tripRating={0}
            />
        );
        const starIcons = renderResult.getAllByTestId("star-icon-stub");
        expect(starIcons).toHaveLength(5);
    });

    it.each([
        [0, 0],
        [0.5, 1],
        [1, 1],
        [2.4, 3],
        [3, 3],
        [3.7, 4],
        [4, 4],
        [4.3, 5],
        [5, 5],
        [8, 5],
    ])("Given %d rating should color %d stars (it rounds up)", (tripRating: number, nbGoldStars: number) => {
        const renderResult = render(
            <TripComponent
                id={0}
                backgroundImgUrl={""}
                title={""}
                duration={0}
                nbCountries={0}
                emissionsOffset={0}
                tripRating={tripRating}
            />
        );
        const starIcons = renderResult.getAllByTestId("star-icon-stub");
        starIcons.forEach((starIcon, index) => {
            const indexToCall = index + 1;
            const expectedColor = indexToCall <= nbGoldStars ? "gold" : "gray.300";
            expect(mockChakraIcon).toHaveBeenNthCalledWith(indexToCall, { color: expectedColor });
        });
    });
});
