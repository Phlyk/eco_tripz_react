import type { Config } from "jest";

const config: Config = {
    setupFilesAfterEnv: ["<rootDir>/tests/jest.setup.ts"],
    testMatch: ["<rootDir>/tests/**/*.test.(js|ts|tsx)"],
    roots: ["<rootDir>/src", "<rootDir>/tests"],
    collectCoverage: true,
    collectCoverageFrom: ["src/**/*.{js,jsx}"],
    coverageDirectory: "coverage",
    testEnvironment: "jsdom",
    transform: {
        "^.+\\.(ts|tsx)$": "ts-jest",
    },
};

export default config;
