import TripListComponent from "./trip/TripList.Component";

export const App = () => (
    <div>
        <TripListComponent />
    </div>
);
