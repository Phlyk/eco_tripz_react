import React from "react";
import ReactDOM from "react-dom/client";
import { App } from "./App";
import "./index.css";

import { ChakraProvider, extendTheme } from "@chakra-ui/react";

const ecoTripzTheme = extendTheme({
    styles: {
        global: {
            body: {
                bg: "gray.100",
            },
        },
    },
    fonts: {
        body: "'Roboto Condensed', sans-serif",
    },
});

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
    <React.StrictMode>
        <ChakraProvider theme={ecoTripzTheme}>
            <App />
        </ChakraProvider>
    </React.StrictMode>
);
