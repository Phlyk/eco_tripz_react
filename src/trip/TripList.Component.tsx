import { SimpleGrid } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Trip } from "./Trip";
import TripComponent from "./Trip.Component";
import { getTrips } from "./Trip.Service";

const TripListComponent: React.FC = () => {
    const [trips, setTrips] = useState<Trip[]>([]);

    useEffect(() => {
        async function fetchTrips() {
            const data = await getTrips();
            setTrips(data);
        }
        fetchTrips();
    }, []);

    return (
        <SimpleGrid
            flexDirection="column"
            alignContent="center"
            textAlign="center"
            maxW={{ xl: "1200px" }}
            m="0 auto"
            minChildWidth="280px"
            spacing="10"
            padding="30px 20px"
        >
            {trips.map((trip: Trip) => (
                <TripComponent {...trip} key={trip.id} />
            ))}
        </SimpleGrid>
    );
};

export default TripListComponent;
