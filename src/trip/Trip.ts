export interface Trip {
    id: number;
    backgroundImgUrl: string;
    title: string;
    duration: number;
    nbCountries: number;
    emissionsOffset: number;
    tripRating: number;
}
