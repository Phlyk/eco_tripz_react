import axios, { AxiosResponse } from "axios";
import { Trip } from "./Trip";

const localApiUrl = "http://localhost:3001/api/trips";

export async function getTrips(): Promise<Trip[]> {
    return axios.get(localApiUrl).then((response: AxiosResponse<string>) => {
        const shmerps = JSON.parse(response.data);
        console.log("[getTrips] response.data: ", shmerps);
        return shmerps;
    });
}
