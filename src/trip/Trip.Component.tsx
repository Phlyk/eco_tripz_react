import { Box, Heading, Text } from "@chakra-ui/react";
import React from "react";
import { Trip } from "./Trip";
import { StarIcon } from "@chakra-ui/icons";

const TripComponent: React.FC<Trip> = ({
    backgroundImgUrl,
    duration,
    emissionsOffset,
    nbCountries,
    title,
    tripRating,
}: Trip): JSX.Element => {
    const kgToTons = 1000;
    const emissionsUnit = emissionsOffset > kgToTons ? "t" : "kg";
    const emissionsOffsetUnit = emissionsOffset > kgToTons ? Number((emissionsOffset / kgToTons).toFixed(2)) : emissionsOffset;
    Array.of(1, 2, 3, 4, 5).map(i => <li key={i}></li>);

    return (
        <Box
            boxShadow="0px 4px 10px gray"
            maxW="sm"
            border="white 0.5rem solid"
            borderRadius="lg"
            overflow="hidden"
            justifyItems={{ base: "center", md: "start" }}
            color="white"
            position="relative"
            bgImage={backgroundImgUrl}
            bgPosition="center"
            bgRepeat="no-repeat"
            bgSize="cover"
            _after={{
                content: '""',
                position: "absolute",
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.4)",
                zIndex: 1,
                mixBlendMode: "darken",
            }}
        >
            <Box mt="1" fontWeight="semibold" lineHeight="tight" noOfLines={1} marginTop="12" position="relative" zIndex="2">
                <Heading size="md" flex="1">
                    {title}
                </Heading>
            </Box>

            <Box as="span" fontSize="sm" position="relative" zIndex="2">
                {nbCountries} Countries, {duration} days
            </Box>

            <Box
                alignItems="center"
                width="75%"
                height="12"
                background="rgb(22,4,36)"
                display="inline-flex"
                borderRadius="0.75rem"
                padding="2"
                marginTop="4"
                boxShadow="0px 0px 1px black"
                position="relative"
                zIndex="2"
            >
                <Text fontSize="sm" flex="1" textAlign="left" paddingLeft="1">
                    <b>Emissions offset:</b>
                </Text>
                <Box as="span" fontSize="sm">
                    {emissionsOffsetUnit} {emissionsUnit} CO<sub>2</sub>e{" "}
                </Box>
            </Box>

            <Box
                color="black"
                alignItems="center"
                width="75%"
                height="12"
                background="white"
                display="inline-flex"
                borderRadius="0.75rem 0.75rem 0 0"
                padding="4"
                marginTop="4"
                boxShadow="0px 0px 1px black"
                position="relative"
                zIndex="2"
            >
                <Text fontSize="sm" flex="1" textAlign="left" paddingLeft="1">
                    <b>Trip rating</b>
                </Text>
                <Box marginRight="1">
                    {calculateStars(tripRating).map((i, index) => (
                        <StarIcon key={index} color={i > 0 ? "gold" : "gray.300"} /> //todo custom icon
                    ))}
                </Box>
                <Box as="span" marginLeft="1">
                    {tripRating}
                </Box>
            </Box>
        </Box>
    );
};

function calculateStars(tripRating: number): number[] {
    return Array.of(1, 2, 3, 4, 5).map(n => {
        if (n <= tripRating) {
            return 1;
        } else if (n > tripRating && n < tripRating + 1) {
            return Number((tripRating % 1).toFixed(2));
        } else {
            return 0;
        }
    });
}

export default TripComponent;

// test should display the number of highlighted stars as per the rating
